const axios = require('axios');
const confing = require('config');
const Markup = require('node-vk-bot-api/lib/markup');
const utils = require('./utils');
const db = require('./database');

module.exports.handle = function (bot) {
    bot.command(utils.getMessage('btn_grade_11'), async (ctx) => {
        utils.sendChatAction(bot, ctx.message.from_id);
        ctx.reply(utils.getMessage('soon'));
        userOnboarding(ctx);
        addUserToDatabases(ctx);
    });

    bot.command(utils.getMessage('btn_grade_10'), async (ctx) => {
        utils.sendChatAction(bot, ctx.message.from_id);
        ctx.reply(utils.getMessage('distant'));
        userOnboarding(ctx);
        addUserToDatabases(ctx);
    });

    bot.command(utils.getMessage('btn_other_grade'), async (ctx) => {
        utils.sendChatAction(bot, ctx.message.from_id);
        ctx.reply(utils.getMessage('mystery'));
        userOnboarding(ctx);
        addUserToDatabases(ctx);
    });

    bot.use(async (ctx, next) => {
        let user = await db.getUser(ctx.message.from_id);
        if (!user) {
            ctx.reply(utils.getMessage('partners'));
            utils.sendChatAction(bot, ctx.message.from_id);
            ctx.reply(utils.getMessage('hello'));
            ctx.reply(utils.getMessage('tutor'));
            ctx.reply(utils.getMessage('grade'), null, Markup.keyboard([
                utils.getMessage('btn_grade_11'),
                utils.getMessage('btn_grade_10'),
                utils.getMessage('btn_other_grade'),
            ]));
        } else {
            next();
        }
    });
}

async function addUserToDatabases(ctx) {
    let user = await db.createUser(ctx.message.from_id);
    axios.post(confing.get('tasksEndpoint'), user);
    axios.post(confing.get('statisticsEndpoint'), user);
}

function userOnboarding(ctx) {
    ctx.reply(utils.getMessage('program'));
    ctx.reply(utils.getMessage('usecase'));
    ctx.reply(utils.getMessage('beginning'), null, Markup.keyboard([
        utils.getMessage('btn_reading'),
        utils.getMessage('btn_grammar'),
    ], {columns: 1}));
}