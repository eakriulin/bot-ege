const axios = require('axios');
const confing = require('config');
const Markup = require('node-vk-bot-api/lib/markup');
const utils = require('./utils');
const db = require('./database');

module.exports.handle = function (bot) {
    bot.command(utils.getMessage('activate_subscription'), (ctx) => {
        utils.sendChatAction(bot, ctx.message.from_id);
        activateSubscription(ctx);
    });
}

async function activateSubscription(ctx) {
    const user = await db.getUser(ctx.message.from_id);
    
    let url = confing.get('subscriptionsEndpoint') + '/activate';
    await axios.post(url, {
        userId: user.internalUserId
    });

    ctx.reply(utils.getMessage('activated'), null, Markup.keyboard([
        utils.getMessage('btn_reading'),
        utils.getMessage('btn_grammar'),
    ], {columns: 1}));
}