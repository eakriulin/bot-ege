const axios = require('axios');
const confing = require('config');
const Markup = require('node-vk-bot-api/lib/markup');
const utils = require('./utils');
const db = require('./database');

module.exports.handle = function (bot) {
    bot.event('message_new', async (ctx) => { 
        utils.sendChatAction(bot, ctx.message.from_id);
        proceedLearning(ctx);
    });
}

async function proceedLearning(ctx) {
    const user = await db.getUser(ctx.message.from_id);

    // Request to the core service to check the user's answer
    let url = confing.get('tasksEndpoint') + '/check_correct_answer';
    let response = await axios.get(url, {data: {
        userId: user.internalUserId, answer: getUserAnswer(ctx)
    }});

    if (typeof response.data !== 'object') return fluentSpeechHandling(ctx);
    
    let message;

    if (response.data.correct) message = utils.getMessage('correct');
    else message = utils.getMessage('wrong')(response.data.answer);

    // Get next task for the user
    url = confing.get('tasksEndpoint') + '/get_next_task';
    let {data} = await axios.get(url, {data: {
        userId: user.internalUserId
    }});

    // Send the previous answer result and the next task (finish learning if the next task is not recieved)
    if (data.question) {
        ctx.reply(message);
        let question, markup;
        [question, markup] = utils.getQuestionWithOptions(data);
        ctx.reply(question, null, markup);
    } else {
        ctx.reply(message);
        message = utils.getMessage('done')(data.correctAmount, data.wrongAmount);
        ctx.reply(message, null, Markup.keyboard([
            utils.getMessage('btn_reading'),
            utils.getMessage('btn_grammar'),
        ], {columns: 1}));
    }
}

function fluentSpeechHandling(ctx) {
    ctx.reply(
        utils.getMessage('fluent_speech'),
        confing.get('keyboardPicture'),
        Markup.keyboard([
            utils.getMessage('btn_reading'),
            utils.getMessage('btn_grammar'),
        ], {columns: 1})
    );
}

function getUserAnswer(ctx) {
    if (ctx.message.hasOwnProperty('payload'))
        return JSON.parse(ctx.message.payload).answer;
    return ctx.message.text;
}