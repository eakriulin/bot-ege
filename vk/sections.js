const axios = require('axios');
const confing = require('config');
const Markup = require('node-vk-bot-api/lib/markup');
const utils = require('./utils');
const db = require('./database');

module.exports.handle = function (bot) {
    bot.command(utils.getMessage('btn_reading'), (ctx) => {
        const exerciseDescription = utils.getMessage('reading_gap');
        utils.sendChatAction(bot, ctx.message.from_id);
        startExercise(ctx, utils.exerciseTypes.reading, exerciseDescription);
    });

    bot.command(utils.getMessage('btn_grammar'), (ctx) => {
        const exerciseDescription = utils.getMessage('grammar_form');
        utils.sendChatAction(bot, ctx.message.from_id);
        startExercise(ctx, utils.exerciseTypes.grammar, exerciseDescription);
    });
}

async function startExercise(ctx, exerciseId, exerciseDescription) {
    const user = await db.getUser(ctx.message.from_id);

    let url = confing.get('tasksEndpoint') + '/initiate_exercise';
    let response = await axios.get(url, {data: {
        exerciseId: exerciseId,
        userId: user.internalUserId
    }});

    if (response.data.isFreeLimitExceeded) {
        sendCommercialMessages(ctx);
    } else {
        url = confing.get('tasksEndpoint') + '/get_next_task';
        response = await axios.get(url, {data: {
            userId: user.internalUserId
        }});
        sendExcerciseMessages(ctx, response.data, exerciseDescription);
    }
}

function sendExcerciseMessages(ctx, data, exerciseDescription) {
    let question, markup;
    [question, markup] = utils.getQuestionWithOptions(data);

    if (question) {
        ctx.reply(utils.getMessage('great'));
        ctx.reply(exerciseDescription);
        ctx.reply(question, null, markup);
    }
}

function sendCommercialMessages(ctx) {
    ctx.reply(utils.getMessage('limit'));
    ctx.reply(utils.getMessage('offer'));
    ctx.reply(utils.getMessage('discount'));
    ctx.reply(utils.getMessage('manual'));
    ctx.reply(utils.getMessage('waiting'), null, Markup.keyboard([
        utils.getMessage('btn_reading'),
        utils.getMessage('btn_grammar'),
    ], {columns: 1}));
}