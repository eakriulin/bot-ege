const mongoose = require('mongoose');
const config = require('config');

const dbHost = config.get('dbHost');
const dbName = config.get('dbName');
mongoose.connect(`mongodb://${dbHost}/${dbName}`, {useNewUrlParser: true, useFindAndModify: false})
    .then(() => console.log('Connected to MongoDB...'))
    .catch((err) => console.error('Could not connect to MongoDB', err));

const appUserMapSchema = new mongoose.Schema({
    appUserId: mongoose.Schema.Types.Mixed,
    app: String,
    internalUserId: mongoose.Schema.ObjectId
});

// AppUserMap class refers to 'appusermaps' collection
const AppUserMap = mongoose.model('AppUserMap', appUserMapSchema);

module.exports.getUser = async function(appUserId) {
    return await AppUserMap
        .findOne({appUserId: appUserId});
}

module.exports.createUser = async function(appUserId) {
    const user = await new AppUserMap({
        appUserId: appUserId,
        app: 'vk'
    }).save();

    return await AppUserMap
        .findByIdAndUpdate(user._id, {
            $set: {internalUserId: user._id}
        }, {new: true});
}