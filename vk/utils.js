const shuffle = require('shuffle-array');
const Markup = require('node-vk-bot-api/lib/markup');
const constants = require('./constants');
const execute = require('node-vk-bot-api/lib/');

module.exports.exerciseTypes = {
    reading: 1,
    grammar: 2
}

module.exports.getQuestionWithOptions = function(body) {
    if (body.exerciseId === this.exerciseTypes.reading) {
        shuffle(body.choices);
        const options = getOptionsForQuestion(body.choices);
        const question = body.question + options;
        const markup = getMarkupForQuestion(body.choices, 2);
        return [question, markup];
    }
    return [body.question, Markup.keyboard([])];
}

module.exports.getMessage = function(message) {
    return constants[message];
}

module.exports.sendChatAction = function(bot, userId) {
    bot.execute('messages.setActivity', {
        user_id: userId,
        type: 'typing'
    })
}

function getOptionsForQuestion(choices) {
    let options = '\n\n';
    choices.forEach((option, i) => {
        options = options + `${i + 1}. ${option}\n`;
    });
    return options;
}

function getMarkupForQuestion(choices, columnsAmount) {
    let buttons = [];
    choices.forEach((option, i) => {
        buttons.push(Markup.button((i + 1).toString(), 'default', {
            answer: option
        }));
    });
    return Markup.keyboard(buttons, {columns: columnsAmount});
}