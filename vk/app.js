const express = require('express');
const bodyParser = require('body-parser');
const VkBot = require('node-vk-bot-api');
const config = require('config');
const registration = require('./registration');
const sections = require('./sections');
const commercial = require('./commercial')
const learning = require('./learning');

const app = express();
const bot = new VkBot({
    token: config.get('token'),
    confirmation: config.get('confirmation')
});

registration.handle(bot);
sections.handle(bot);
commercial.handle(bot);
learning.handle(bot);

if (app.get('env') === 'development') {
    bot.startPolling();
    console.log('Long Polling is active...');
} else {
    app.use(bodyParser.json());
    app.post('/', bot.webhookCallback);
    app.listen(config.get('vkPort'), () => console.log(`Listening on port ${config.get('vkPort')}...`));
}