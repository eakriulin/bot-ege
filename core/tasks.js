const axios = require('axios');
const express = require('express');
const confing = require('config');
const db = require('./database');

const router = express.Router();

const TASKS_PER_TRAINING = 5;
const FREE_AMOUNT_OF_TRAININGS = 3;

router.post('/', async (req, res) => {
    const user = await db.createUser(req.body.internalUserId);
    res.send(user);
});

router.get('/initiate_exercise', async (req, res) => {
    let user = await db.getUser(req.body.userId);

    let url = confing.get('statisticsEndpoint');
    let {data} = await axios.get(url, {data: {
        userId: req.body.userId
    }});

    if (isFreeLimitExceeded(user, data.statistics)) {
        res.send(getLimitExceededObj(data.statistics));
    } else {
        user = await db.updateUser(req.body.userId, {
            'currentExercise.exerciseId': req.body.exerciseId
        });
        res.send(user);
    }
});

router.get('/get_next_task', async (req, res) => {
    const user = await db.getUser(req.body.userId);
    
    if (user.currentExercise.counter >= TASKS_PER_TRAINING) {
        exerciseStatistic = getExerciseStatistic(user);

        saveExerciseStatistics(user, exerciseStatistic);
        stopLearningProcess(user);

        res.send(exerciseStatistic);
    } else {
        const exerciseId = user.currentExercise.exerciseId;
        const taskId = user.exercisesStatus
            .find((item) => item.exerciseId === exerciseId)
            .taskId;
        const task = await db.getTask(exerciseId, taskId);
        if (!task) stopLearningProcess(user);
        res.send(task);
    }
});

router.get('/check_correct_answer', async (req, res) => {
    const user = await db.getUser(req.body.userId);
    if (!user.currentExercise.exerciseId) {
        res.sendStatus(200);
    } else {
        const exerciseId = user.currentExercise.exerciseId;
        const taskId = user.exercisesStatus
            .find((item) => item.exerciseId === exerciseId)
            .taskId;
        const task = await db.getTask(exerciseId, taskId);
        
        isCorrect = req.body.answer.toLowerCase() === task.choices[0].toLowerCase();
        await updateUserProgress(user, isCorrect);
        res.send(getAnswerExplanation(task, isCorrect));
    }
});

router.get('/get_section_names', async (req, res) => {
    const sectionsNames = await db.getSectionsNames();
    res.send(sections);
});

router.get('/get_exercise_names', async (req, res) => {
    const exercisesNames = await db.getExercisesNames();
    res.send(exercises);
});

function saveExerciseStatistics(user, exerciseStatistic) {
    let url = confing.get('statisticsEndpoint');
    axios.put(url, {
        userId: user.userId, exerciseStatistics: exerciseStatistic
    });
}

function stopLearningProcess(user) {
    db.updateUser(user.userId, {
        'currentExercise.exerciseId': null,
        'currentExercise.counter': 0,
        'currentExercise.correctAmount': 0,
        'currentExercise.wrongAmount': 0
    });
}

async function updateUserProgress(user, isCorrect) {
    if (isCorrect) user.currentExercise.correctAmount ++;
    else user.currentExercise.wrongAmount ++;

    user.exercisesStatus.forEach(item => {
        if (item.exerciseId === user.currentExercise.exerciseId) item.taskId ++;
    });
    user.currentExercise.counter ++;
    
    await db.updateUser(user.userId, user);
}

function getAnswerExplanation(task, isCorrect) {
    return {
        correct: isCorrect,
        answer: task.choices[0],
        explanation: task.explanation
    }
}

function getExerciseStatistic(user) {
    return {
        exerciseId: user.currentExercise.exerciseId,
        correctAmount: user.currentExercise.correctAmount,
        wrongAmount: user.currentExercise.wrongAmount,
        createdAt: new Date()
    }
}

function getLimitExceededObj(statistics) {
    return {
        isFreeLimitExceeded: true,
        tasksAmount: statistics.length * TASKS_PER_TRAINING
    }
}

function isFreeLimitExceeded(user, statistics) {
    return (statistics.length >= FREE_AMOUNT_OF_TRAININGS && !user.subscription)
}

module.exports = router;