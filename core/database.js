const mongoose = require('mongoose');
const config = require('config');

const dbHost = config.get('dbHost');
const dbName = config.get('dbName');
mongoose.connect(`mongodb://${dbHost}/${dbName}`, {useNewUrlParser: true, useFindAndModify: false})
    .then(() => console.log('Connected to MongoDB...'))
    .catch((err) => console.error('Could not connect to MongoDB', err));

const userSchema = new mongoose.Schema({
    userId: mongoose.Schema.ObjectId,
    exercisesStatus: {
        type: Array,
        default: [{exerciseId: 1, taskId: 1}, {exerciseId: 2, taskId: 1}]},
    currentExercise: {
        type: Object,
        default: {exerciseId: null, counter: 0, correctAmount: 0, wrongAmount: 0}},
    subscription: {type: Boolean, default: false},
    registeredAt: {type: Date, default: Date.now}
});

// AppUserMap class refers to 'users' collection
const User = mongoose.model('User' ,userSchema);

module.exports.getUser = async function(userId) {
    return await User
        .findOne({userId: userId});
}

module.exports.createUser = async function(userId) {
    return await new User({
        userId: userId
    }).save();
}

module.exports.updateUser = async function(userId, update) {
    await User
        .updateOne({userId: userId}, {
            $set: update
        });
    return await User
        .findOne({userId: userId});
}

const sectionSchema = new mongoose.Schema({
    sectionId: Number,
    name: String,
});

// Section class refers to 'sections' collection
const Section = mongoose.model('Section', sectionSchema);

module.exports.getSections = async function() {
    return await Section
        .find({});
}

module.exports.getSectionsNames = async function() {
    return await Section
        .find({})
        .select({name: 1});
}

module.exports.createSection = async function(data) {
    return await new Section(data).save();
}

const exerciseSchema = new mongoose.Schema({
    sectionId: Number,
    exerciseId: Number,
    name: String
});

// Exercise class refers to 'exercises' collection
const Exercise = mongoose.model('Exercise', exerciseSchema);

module.exports.getExercises = async function() {
    return await Exercise
        .find({});
}

module.exports.getExercisesNames = async function() {
    return await Exercise
        .find({})
        .select({name: 1});
}

module.exports.createExercise = async function(data) {
    return await new Exercise(data).save();
}

const taskSchema = new mongoose.Schema({
    exerciseId: Number,
    taskId: Number,
    question: String,
    choices: Array,
    explanation: String 
});

// Task class refers to 'tasks' collection
const Task = mongoose.model('Task', taskSchema);

module.exports.getTask = async function(exerciseId, taskId) {
    return await Task
        .findOne({
            exerciseId: exerciseId,
            taskId: taskId
        });
}

module.exports.createTask = async function(data) {
    return await new Task(data).save();
}