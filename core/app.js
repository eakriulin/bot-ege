const express = require('express');
const config = require('config');
const tasks = require('./tasks');
const subscriptions = require('./subscriptions');

const app = express();
app.use(express.json());

app.use('/api/tasks', tasks);
app.use('/api/subscriptions', subscriptions);

const port = config.get('corePort')
app.listen(port, () => console.log(`Listening on port ${port}...`));