const axios = require('axios');
const express = require('express');
const confing = require('config');
const db = require('./database');

const router = express.Router();

router.post('/activate', async (req, res) => {
    const user = await db.updateUser(req.body.userId, {
        'subscription': true
    });
    res.send(user);
});

module.exports = router;