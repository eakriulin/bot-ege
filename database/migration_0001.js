const db = require('../core/database');

const sections = [{
    sectionId: 1,
    name: '📖 Чтение',
}, {
    sectionId: 2,
    name: '🎓 Грамматика и лексика'
}];

const exercises = [{
    sectionId: 1,
    exerciseId: 1,
    name: '📖 Чтение',
}, {
    sectionId: 2,
    exerciseId: 2,
    name: '🎓 Грамматика и лексика'
}];

const tasks = [

// Exercise 1 placeholder
{
    exerciseId: 1,
    taskId: 1,
    question: 'Здесь я задаю первый вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 1,
    taskId: 2,
    question: 'Здесь я задаю второй вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 1,
    taskId: 3,
    question: 'Здесь я задаю третий вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 1,
    taskId: 4,
    question: 'Здесь я задаю четвертый вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 1,
    taskId: 5,
    question: 'Здесь я задаю пятый вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 1,
    taskId: 6,
    question: 'Здесь я задаю шестой вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 1,
    taskId: 7,
    question: 'Здесь я задаю седьмой вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 1,
    taskId: 8,
    question: 'Здесь я задаю восьмой вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 1,
    taskId: 9,
    question: 'Здесь я задаю девятый вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 1,
    taskId: 10,
    question: 'Здесь я задаю десятый вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, 

// Exercise 2 placeholder
{
    exerciseId: 2,
    taskId: 1,
    question: 'Здесь я задаю первый вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 2,
    taskId: 2,
    question: 'Здесь я задаю второй вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 2,
    taskId: 3,
    question: 'Здесь я задаю третий вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 2,
    taskId: 4,
    question: 'Здесь я задаю четвертый вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 2,
    taskId: 5,
    question: 'Здесь я задаю пятый вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 2,
    taskId: 6,
    question: 'Здесь я задаю шестой вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 2,
    taskId: 7,
    question: 'Здесь я задаю седьмой вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 2,
    taskId: 8,
    question: 'Здесь я задаю восьмой вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 2,
    taskId: 9,
    question: 'Здесь я задаю девятый вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}, {
    exerciseId: 2,
    taskId: 10,
    question: 'Здесь я задаю десятый вопрос и жду выбор ответа 👇',
    choices: ['1', '2', '3', '4'],
    explanation: 'Потому что 🤘'
}];

function run() {
    sections.forEach((section) => {
        db.createSection(section);
    });
    exercises.forEach((exercise) => {
        db.createExercise(exercise);
    });
}

run();