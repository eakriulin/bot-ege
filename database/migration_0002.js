const db = require('../core/database');
const readXlsxFile = require('read-excel-file/node');

function run() {
    const path = './database/files/content_1.xlsx';

    readXlsxFile(path, {sheet: 1}).then((data) => {
        data.forEach((task, i) => {
            let exerciseId = 2;
            let taskId = i + 1;
            let question = task[0].trim();
            let choices = [task[1].trim(), task[2].trim(), task[3].trim(), task[4].trim()];
            let explanation = task[5].trim();
            addTask(exerciseId, taskId, question, choices, explanation);
        });
    });

    readXlsxFile(path, {sheet: 2}).then((data) => {
        data.forEach((task, i) => {
            let exerciseId = 1;
            let taskId = i + 1;
            let question = task[0].trim();
            let choices = [task[1].trim(), task[2].trim(), task[3].trim(), task[4].trim()];
            let explanation = task[5].trim();
            addTask(exerciseId, taskId, question, choices, explanation);
        });
    });
}

function addTask(exerciseId, taskId, question, choices, explanation) {
    db.createTask({
        exerciseId: exerciseId,
        taskId: taskId,
        question: question,
        choices: choices,
        explanation: explanation
    });
}

run();
console.log('Done...');