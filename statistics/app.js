const express = require('express');
const config = require('config');
const statistics = require('./statistics');

const app = express();
app.use(express.json());

app.use('/api/statistics', statistics);

const port = config.get('statisticsPort')
app.listen(port, () => console.log(`Listening on port ${port}...`));