const mongoose = require('mongoose');
const config = require('config');

const dbHost = config.get('dbHost');
const dbName = config.get('dbName');
mongoose.connect(`mongodb://${dbHost}/${dbName}`, {useNewUrlParser: true, useFindAndModify: false})
    .then(() => console.log('Connected to MongoDB...'))
    .catch((err) => console.error('Could not connect to MongoDB', err));

const statisticsSchema = new mongoose.Schema({
    userId: mongoose.Schema.ObjectId,
    statistics: {type: Array, default: []}
});

// AppUserMap class refers to 'users' collection
const Statistics = mongoose.model('Statistics', statisticsSchema);

module.exports.createUserStatistics = async function(userId) {
    return await new Statistics({
        userId: userId
    }).save();
}

module.exports.addExerciseStatistics = async function(userId, exerciseStatistics) {
    await Statistics
        .updateOne({userId: userId}, {
            $push: {statistics: exerciseStatistics}
        });
    return await Statistics
        .findOne({userId: userId});
}

module.exports.getUserStatistics = async function(userId) {
    return await Statistics
        .findOne({userId: userId});
}