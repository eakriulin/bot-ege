const express = require('express');
const db = require('./database');

const router = express.Router();

router.post('/', async (req, res) => {
    const userStatistics = await db.createUserStatistics(req.body.internalUserId);
    res.send(userStatistics);
});

router.put('/', async (req, res) => {
    const exerciseStatistics = await db.addExerciseStatistics(req.body.userId, req.body.exerciseStatistics);
    res.send(exerciseStatistics);
});

router.get('/', async (req, res) => {
    const userStatistics = await db.getUserStatistics(req.body.userId);
    res.send(userStatistics);
});

module.exports = router;